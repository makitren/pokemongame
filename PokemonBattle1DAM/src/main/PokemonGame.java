    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import classes.Pokemon;
import classes.Pokemon.PokemonType;
import static classes.Pokemon.PokemonType.WATER;
import classes.User;
import classes.User.AccessLevel;
import static classes.User.AccessLevel.BASIC;
import static classes.User.AccessLevel.PREMIUM;
import exceptions.InvalidGenreException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cenec
 */
public class PokemonGame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Variables needed to work
        Scanner sc=new Scanner(System.in);
        Connection connection=null;
                try {
             connection=DriverManager.getConnection(
                    "jdbc:mysql://85.214.120.213:3306/pokemonbattle1dam"
                    ,"1dam","1dam");
        } catch (SQLException ex) {
                    System.err.println("La conexion  a bd ha fallado");
        }
        
        String menu="\n\nChoose action by number:"
                +"\n\t0 - Exit game"
                +"\n\t1 - Register new User"
                +"\n\t2 - Login User";
        int action=0;
        User user=null;
        do{
            if(user!=null){
                    menu="\n\nHello, "+user.getName()+". Choose action by number:"
                +"\n\t0 - Exit game";
                    if(user.getPokemon()==null){
                        menu+="\n\t1 - Register new pokemon";
                    }else{
                        //11 - Concatenate another option 1 - Show pokemon
                        menu=menu+ "\n\t1 - Show Pokemon";
                    }
            }
            System.out.println(menu);
            action=Integer.parseInt(sc.nextLine());
            switch(action){
                case 0:
                    System.out.println("Bye");
                    break;
                case 1:
                    //if user is still null, the user hasn't been registered nor logged in
                    
                    if(user==null){
                        user=registerUser(sc,connection);
                    }else{
                        if (user.getPokemon()==null) {
                            System.out.println("Let's register your pokemon!");
                        registerPokemon(sc,connection,user);
                        }else{
                            //12- Modify case 1 on the switch, and if it finds that user!=null&&user.getPokemon()!=null execute a new fucntion in pokemon class: public String print() , that returns all the pokemon's data.
                            System.out.println(user.getPokemon().getPokemonData(user.getPokemon()));
                        }
                        
                    }
                    break;
                case 2:
                    user=loginUser(sc,connection);
                 break;
                default:
                    System.out.println("Invalid option choosen");
                    break;
            }
        }while(action!=0);
    }
    
    public static void registerPokemon(Scanner sc, Connection conn, User user){
        try {
            //1 - Ask for all of pokemon data via Scanner
            System.out.println("Introduce el nombre de tu pokemon");
            String nombre=sc.nextLine();
            System.out.println("Introduce el genero de tu pokemon");
            char genero=sc.nextLine().charAt(0);
            System.out.println("Introduce una descripcion de tu pokemon");
            String descripcion=sc.nextLine();
            System.out.println("Introduce los puntos de vida de tu pokemon");
            short hp=(short)Integer.parseInt(sc.nextLine());
            System.out.println("Introduce la especie de tu pokemon");
            String especie=sc.nextLine();
           // Pokemon pokemones=new Pokemon(nombre,genero,descripcion,Pokemon.PokemonType.WATER,hp,especie);
            Statement registrarUsuario=conn.createStatement();
            //2 - Create an statement object and insert into pokemon table.  (you can skip id , as it is auto-incremented)
          // Pokemon actual=new Pokemon(nombre,genero,descripcion,WATER,hp,especie);
           boolean genIns=true;
           if(genero=='m'){
               genIns=false;
           }
            PreparedStatement loginStatement=conn.prepareStatement(
            "insert into pokemon (name,genre,description,type,species,level,lifePoints,xp"
                            + ") values('"+nombre+"',"+genIns+",'"+descripcion+
                            "','"+WATER+"','"+especie+
                            "','"+1+"','"+hp+"',"+0+")"
                            );
            loginStatement.executeUpdate();
            loginStatement.close();
            System.out.println("Pokemon insertado");
            
            //3 - Query the newly created pokemon id (query max id from pokemon table)
            
            System.out.println("Buscar al pokemon");
             //loginStatement=conn.prepareStatement("select * from pokemon where name = "+"'"+nombre+"'");
                System.out.println(loginStatement);
                Statement smt2=conn.createStatement();
                ResultSet resultado=smt2.executeQuery("select max(id) from pokemon" );
                resultado.next();
                System.out.println(loginStatement);
                int idMax=resultado.getInt("max(id)");
                loginStatement.close();    
            
            
            //4 - Create a java Pokemon object with the read data and the queried id
            
            Pokemon alfredopokemon=new Pokemon(idMax,nombre,genero,descripcion,Pokemon.PokemonType.WATER,especie,hp);
            
            
            
            //5 - Use setPokemon in User class to Link the pokemon to the user in java
            user.setPokemon(alfredopokemon);
            
            //6 - Reuse Statement or create a new one to insert into pokemonUser table, where you link pokemon and user.
             PreparedStatement logStatement=conn.prepareStatement(
                     "insert into pokemonUser(user,pokemonId"
                             + ")values('"+user.getName()+"',"+alfredopokemon.getId()+")");
             logStatement.executeUpdate();
               } catch (SQLException ex) {
            ex.printStackTrace();
        } catch (InvalidGenreException ex) {
            ex.printStackTrace();
        }
            
    }
    
    public static User registerUser(Scanner sc, Connection conn){
         try {
         System.out.println("Tell me your username:");
        String username=sc.nextLine();
        System.out.println("Choose your Password:");
        String password=sc.nextLine();
        System.out.println("Choose your genre (m/f)");
        char genreChar=sc.nextLine().charAt(0);
        System.out.println("Tell me about you:");
        String description=sc.nextLine();
        
       
            
            User actual=new User(username,genreChar,
                    description,password,AccessLevel.BASIC,null);
            //we're goint to persist the user in the database
            //so we can log in as him/her later
            Statement registerStatement=conn.createStatement();
            registerStatement.executeUpdate(
                    "insert into user (name,genre,description,password,"
                            + "lastConnection,accessLevel) values('"+username+"',"
                            + "'"+genreChar+"','"+description+
                            "','"+password+"',"+
                            //Abbreviated if. This means:
                            //(condition?executesIfTrue:executesifFalse)
                            // ? and : are just separators
                            (actual.getLastConnection()!=null?
                            "'"+actual.getLastConnection()+"'":
                            "null")
                            +",'BASIC')");
            registerStatement.close();
            return actual;
         } catch (InvalidGenreException ex) {
                System.err.println(ex.getMessage());
                registerUser(sc,conn);
        } catch (SQLException ex) {
             System.err.println("SQL exception");
             ex.printStackTrace();
        }
         return null;
    }
    
    public static User loginUser(Scanner sc, Connection conn){
        try {
            System.out.println("What's your username?");
            String username=sc.nextLine();
            System.out.println("And your password?");
            String password=sc.nextLine();
            //First thing to do would be to retrieve the whole
            //user info from db.
             //We're using the wildcard (something you can replace
            //by anything) ? in the following statement. the String 
            //array is used to give values to the wildcards as they
            //appear (position 0 replaces the first ? , position 1 replaces
            //the second ? and so on...
            PreparedStatement loginStatement=
                    conn.prepareStatement(
                            "select * from user where name=? "
                                    + "and password=? ");
                            loginStatement.setString(1, username);
                            loginStatement.setString(2, password);
            ResultSet foundUser=loginStatement.executeQuery();

            if(foundUser.next()){ //User is found
                 System.out.println("Login succesful!");            
                    //we get our recovered user and put it into a Java Object
                AccessLevel al=null;
                if(foundUser.getString("accessLevel")
                        .equals("BASIC")){
                    al=BASIC;
                }else if (foundUser.getString("accessLevel")
                        .equals("PREMIUM")){
                    al=PREMIUM;
                }else{
                    System.err.println("Invalid Access Level on Database");
                }
                User actual=new User(foundUser.getString("name"),
                foundUser.getString("genre").charAt(0),
                foundUser.getString("description"),
                foundUser.getString("password"),
                al,null);
                foundUser.close();
                System.out.println("Hello, "+actual.getName()+", login successful!");
                
                
                //7 - Create or reuse a Statement to query the pokemon linked to the user
              Statement sr=conn.createStatement();
              ResultSet pokemonId=sr.executeQuery("SELECT pokemonId"
                      + " FROM pokemonUser"
                      + " WHERE user='"+actual.getName()+"'");
                      Pokemon nuevoPokemon=null;
                        if(pokemonId.next()){
                           
                //8 - Create or reuse a Statement to query all pokemon data for the recovered id
                // (select * from pokemon where id= ?)
                ResultSet pokemonData=sr.executeQuery("SELECT *"
                        + " FROM pokemon"
                        + " WHERE id="+pokemonId.getInt("pokemonId")+"");
                            if (pokemonData.next()) {
                               
                //9 - Create a Pokemon java object with all recovered data from the previous step (or null if the user doesn't have any)
                 PokemonType pt=null;
                                switch(pokemonData.getString("type")){
                                    case "FIRE":
                                        pt=PokemonType.FIRE;
                                        break;
                                     case "WATER":
                                        pt=PokemonType.WATER;
                                        break;
                                    case "PLANT":
                                        pt=PokemonType.PLANT;
                                        break; 
                                    default:
                                        System.err.println("Tipo de pokemon Invalido");                                           
                                }
                                nuevoPokemon=new Pokemon(pokemonData.getInt("id"),pokemonData.getString("name"),(pokemonData.getBoolean("genre")==false?'M':'F'),pokemonData.getString("description"),pt,pokemonData.getString("species"),pokemonData.getShort("lifePoints"));
                            }
                }else{
                            System.err.println("El usuario no tiene pokemon en la base de datos");
                        }
                
                //10 - (only if step 8 didn't return null) Use the newly created Pokemon variable in the User's constructor last argument
                   if (nuevoPokemon!=null) {
                    actual.setPokemon(nuevoPokemon);
                }
                
               
               return actual;
            }else{ //User is not found
                    System.err.println("Invalid username and password");
                    loginUser(sc,conn);
            }
        } catch (SQLException ex) {
            System.err.println("SQL Exception");
            ex.printStackTrace();
        } catch (InvalidGenreException ex) {
            //This should never happen because
            //we make sure when we register the user
            //that genre can only have m or f
            System.err.println(ex.getMessage());
            System.err.println("Holy Shit! This should have never happened!");
            //We use an assert because this is a theoretically impossible situation
            assert true: "Holy Shit! This should have never happened!";
        }
        //If login is incorrect, return null user
        return null;
    }
    
}
